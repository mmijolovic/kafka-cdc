from kafka import KafkaProducer
from time import sleep
import random

producer = KafkaProducer(retries=10, acks=1)

for index in range(100000):
    producer.send('test-topic2', '{}'.format(index).encode('utf-8'))
    producer.flush()
    print("Sent message {}".format(index))
    # sleep(random.random())


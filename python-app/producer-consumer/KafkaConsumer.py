from kafka import KafkaConsumer
from time import sleep
import random

consumer = KafkaConsumer('test-topic2',
                         group_id='test_group',
                         bootstrap_servers=['localhost:9092'],
                         enable_auto_commit=True,
                         auto_commit_interval_ms=1)

last_index = None

for message in consumer:

    current_index = int(message.value)
    if last_index is not None and current_index > last_index and current_index != last_index + 1:
        print("Error!!! Missing messages betweeen {} and {}".format(last_index, current_index))
        break

    print("%s:%d:%d: value=%s" % (message.topic, message.partition, message.offset, message.value))

    last_index = current_index
    # sleep(random.random())

#!/usr/bin/env bash

until psql -h localhost -U postgres -c '\l' &>/dev/null; do
  echo >&2 "$(date +%Y%m%dt%H%M%S) Postgres is unavailable - sleeping"
  sleep 1
done

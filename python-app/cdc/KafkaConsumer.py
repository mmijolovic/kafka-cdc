from kafka import KafkaConsumer

import json

consumer = KafkaConsumer('dbserver1.public.messages',
                         group_id='test_group',
                         bootstrap_servers=['localhost:9092'],
                         enable_auto_commit=True,
                         auto_commit_interval_ms=10)

last_index = None

for message in consumer:
    print(message.key.decode('utf-8'))

    value = message.value

    if not value:
        print("<No value - this is a tombstone message>\n")
        continue

    value_json = json.loads(value.decode('utf-8'))
    print(json.dumps(value_json, indent=4))
    print()

#!/usr/bin/env bash

bash scripts/hard-reset.sh

echo "Waiting for postgres to become available.".
bash scripts/wait-postgres-up.sh

echo "Postgres is up. Initializing database".

# Initialize database
bash scripts/init-db.sh &>/dev/null
bash scripts/init-tables.sh &>/dev/null

# Initialize KafkaConnect connector
echo "Waiting for KafkaConnect to become available."
bash debezium/set-debezium-connector.sh
echo "KafkaConnect connector set."

echo "The system is running."
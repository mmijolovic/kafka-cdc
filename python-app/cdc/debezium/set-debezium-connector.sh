#!/usr/bin/env bash

until curl -X POST \
        -H "Accept:application/json" \
        -H "Content-Type:application/json" \
        localhost:8083/connectors/ \
        -d @debezium/debezium-connector.json --silent &>/dev/null; do

        echo "Waiting. The debezium is not up."
        sleep 1
done
